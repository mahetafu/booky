from django.contrib import admin

from .models import BookBorrow

# Register your models here.

admin.site.register(BookBorrow)