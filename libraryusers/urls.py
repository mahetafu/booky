from django.urls import path

from . import views

urlpatterns = [
    path('', views.users, name='library-users'),
    
    path('create-user/', views.create_user, name='create-user'),
    path('update-user/<int:user_id>/', views.update_user, name='update-user'),
    path('delete-user/<int:user_id>/', views.delete_user, name='delete-user'),
]
