from django.db import models

# Create your models here.

class LibraryUser(models.Model):
    name = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    mail = models.EmailField(unique=True)

    class Meta:
        ordering = ['lastname', 'name']

    def __str__(self) -> str:
        return f'{self.name} {self.lastname} ({self.mail})'
