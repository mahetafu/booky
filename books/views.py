from django.shortcuts import render, redirect
from http.client import HTTPResponse
from django import forms

from .models import Author, Book
from .forms import CreateBookFrom, UpdateBookFrom

# Create your views here.

redirect_url = 'books'
template_path = 'books/books.html'
form_template_path = 'books/book-form.html'

def create_book(request) -> HTTPResponse:
    return edit_book(request, form_template_path, redirect_url, CreateBookFrom)

def books(request) -> HTTPResponse:
    context = { 'books': Book.objects.all(), }
    return render(request, template_path, context)

def update_book(request, book_id):
    book = Book.objects.get(id=book_id)
    return edit_book(request, form_template_path, redirect_url, UpdateBookFrom, book, 'Update')

def delete_book(request, book_id):
    to_del = Book.objects.get(id=book_id)
    to_del.delete()
    return redirect(redirect_url)



### utils
def edit_book(request, template_path, redirect_url, book_form:forms.ModelForm, \
    book_to_edit:Book = None, edit_action:str = 'Create') -> HTTPResponse:
    edit_form, was_edited = obtain_form(request, book_form, book_to_edit)

    if was_edited:
        return redirect(redirect_url)    

    context = {
        'form': edit_form,
        'view_text': edit_action,
    }

    return render(request, template_path, context)

def obtain_form(request, book_form: forms.ModelForm, \
    book_to_edit: Book = None) -> tuple([object, bool]):
    edit_form = book_form(instance=book_to_edit)
    was_edited = False

    if request.method == 'POST':
        edit_form = book_form(request.POST, instance=book_to_edit)
        
        if edit_form.is_valid():
            edit_form.save()
            was_edited = True

    return edit_form, was_edited
