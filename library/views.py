from typing import Iterable
from django.shortcuts import render, redirect
from http.client import HTTPResponse
from django.utils import timezone 
from django.db.models.query import QuerySet

from books.models import Book
from .models import BookBorrow
from .forms import CreateBookBorrowFrom

# Create your views here.

borrowed_redirect_url = 'borrowed-books'

available_template_path = 'library/available-books.html'
borrowed_template_path = 'library/borrowed-books.html'
overdue_template_path = 'library/overdue-books.html'

borrow_form_template_path = 'library/borrow-book-form.html'

def available_books(request) -> HTTPResponse:
    borrowed_books = BookBorrow.objects.filter(date_returned__isnull=True)
    available_books = Book.objects.exclude(id__in=[value.book.id for value in borrowed_books])

    context = { 'available_books': available_books, }
    return render(request, available_template_path, context)

def borrow_book(request, book_id):
    borrow = BookBorrow()
    borrow.book = Book.objects.get(id=book_id)

    return show_borrow_view(request, borrow_form_template_path, borrowed_redirect_url, borrow, \
        CreateBookBorrowFrom)

def borrowed_books(request) -> HTTPResponse:
    borrowed_books = BookBorrow.objects.filter(date_returned__isnull=True)
    context = { 'borrowed_books': borrowed_books, }
    return render(request, borrowed_template_path, context)

def return_book(request, borrow_id) -> HTTPResponse:
    borrow = BookBorrow.objects.get(id=borrow_id)
    borrow.date_returned = timezone.now().date()
    borrow.save()

    return redirect(borrowed_redirect_url)

def overdue_books(request) -> HTTPResponse:
    overdue_books_data = obtain_overdue_books()
    context = { 'overdue_books_data': overdue_books_data, }
    return render(request, overdue_template_path, context)



### utils
def show_borrow_view(request, template_path: str, redirect_url: str, borrow: BookBorrow, \
    borrow_form: CreateBookBorrowFrom) -> HTTPResponse:
    create_form, was_created = obtain_form(request, borrow_form, borrow)

    if was_created:
        return redirect(redirect_url)

    context = { 'form': create_form, }
    return render(request, template_path, context)

def obtain_form(request, borrow_form: object, borrow: BookBorrow) -> tuple([object, bool]):
    create_form = borrow_form(instance=borrow)
    was_updated = False

    if request.method == 'POST':
        create_form = borrow_form(request.POST, instance=borrow)
        
        if create_form.is_valid():
            create_form.save()
            was_updated = True

    return create_form, was_updated

def obtain_overdue_books() -> list():
    date_today = timezone.now().date()
    overdue_books = BookBorrow.objects.filter(date_returned__isnull=True, \
        date_return__lt=date_today)
    
    overdue_with_days = add_days_late(overdue_books, date_today)

    return overdue_with_days

def add_days_late(overdue_books: QuerySet, date_today) -> list():
    overdue_with_days = list()

    for overdue in overdue_books:
        days_as_date = (date_today - overdue.date_return)

        overdue_with_days.append({
            'overdue': overdue,
            'days_late': days_as_date.days
        })

    return overdue_with_days
