from django import forms

from .models import  BookBorrow

class CreateBookBorrowFrom(forms.ModelForm):
    class Meta:
        model = BookBorrow
        fields = [
            'book',
            'borrower',
            'date_borrowed',
            'date_return',
        ]
