from multiprocessing import AuthenticationError
from django.shortcuts import render, redirect
from http.client import HTTPResponse

from .models import Author
from .forms import CreateAuthorFrom

# Create your views here.

redirect_url = 'authors'
template_path = 'authors/authors.html'
form_template_path = 'authors/author-form.html'

def create_author(request) -> HTTPResponse:
    return edit_author(request, form_template_path, redirect_url)

def authors(request) -> HTTPResponse:
    context = { 'authors': Author.objects.all(), }
    return render(request, template_path, context)

def update_author(request, author_id) -> HTTPResponse:
    author = Author.objects.get(id=author_id)
    return edit_author(request, form_template_path, redirect_url, author, 'Update')

def delete_author(request, author_id):
    to_del = Author.objects.get(id=author_id)
    to_del.delete()
    return redirect(redirect_url)



### utils
def edit_author(request, template_path, redirect_url, \
    author_to_edit:Author = None, edit_action:str = 'Create') -> HTTPResponse:
    edit_form, was_edited = obtain_form(request, CreateAuthorFrom, author_to_edit)

    if was_edited:
        return redirect(redirect_url)    

    context = {
        'form': edit_form,
        'view_text': edit_action,
    }

    return render(request, template_path, context)

def obtain_form(request, author_form: CreateAuthorFrom, \
    author_to_edit: Author = None) -> tuple([object, bool]):
    edit_form = author_form(instance=author_to_edit)
    was_edited = False

    if request.method == 'POST':
        edit_form = author_form(request.POST, instance=author_to_edit)
        
        if edit_form.is_valid():
            edit_form.save()
            was_edited = True

    return edit_form, was_edited
