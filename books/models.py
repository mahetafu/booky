from django.db import models

from authors.models import Author

# Create your models here.

class Book(models.Model):
    name = models.CharField(max_length=100)
    isbn = models.IntegerField(default=0, unique=True)
    authors = models.ManyToManyField(Author)
    
    class Meta:
        ordering = ['isbn']

    def __str__(self) -> str:
        return f'{self.isbn} - {self.name}'
