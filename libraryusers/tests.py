from django.test import TestCase

from .models import LibraryUser

# Create your tests here.

class LibraryUsersTestCase(TestCase):

    def setUp(self) -> None:
        test_user = LibraryUser(name='Stephen', lastname='King', mail='king.stephen@joke.fun')
        test_user.save()

        return super().setUp()
    

    def test_http_create_library_user(self):
        context = {
            'name': 'Johan', 
            'lastname': 'Rowling',
            'mail': 'rowling.johan@joke.fun'
        }
        response = self.client.post('/library-users/create-user', context, follow=True)

        status_code = response.status_code

        self.assertEqual(status_code, 200)

    def test_http_update_library_user(self):
        context = {
            'name': 'Johan', 
            'lastname': 'Rowling'
        }
        response = self.client.post('/library-users/update-user/1', context, follow=True)

        status_code = response.status_code

        self.assertEqual(status_code, 200)

    def test_http_delete_library_user(self):
        context = {}
        response = self.client.post('/library-users/delete-user/1', context, follow=True)

        status_code = response.status_code

        self.assertEqual(status_code, 200)