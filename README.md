# Booky

A simple library management Django app for AgroIT Test

## Installation

### 0. Go to destination directory
cd ~/Desktop

mkdir ./destination_dir

cd ./destination_dir

### 1. Clone GIT repository
git clone https://gitlab.com/mahetafu/booky.git

cd ./booky

### 2. Create a Virtual Env and Run it
#### Create
python3 -m venv ./.venv/

#### Run (Activate)
source ./.venv/bin/activate

### 3. Install all dependencies
pip install -r ./requirements.txt

### 4. Make DB Migrations
python3 manage.py makemigrations

#### If "Make Migrations" did not make the apps migrations; RUN THIS!
python3 manage.py makemigrations authors books libraryusers library

python3 manage.py migrate

### 5. Create Admin user
python3 manage.py createsuperuser

### 6. Run dev server
python3 manage.py runserver localhost:8000

## Authors and acknowledgment
Marcos Talento (talento.marcos@gmail.com)

## License
MIT

