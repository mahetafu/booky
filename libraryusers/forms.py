from django import forms

from .models import LibraryUser

class CreateLibraryUserFrom(forms.ModelForm):
    class Meta:
        model = LibraryUser
        fields = [
            'name',
            'lastname',
            'mail',
        ]

class UpdateLibraryUserFrom(forms.ModelForm):
    class Meta:
        model = LibraryUser
        fields = [
            'name',
            'lastname',
        ]
