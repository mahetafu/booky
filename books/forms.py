from django import forms

from .models import Book

class CreateBookFrom(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            'name',
            'isbn',
            'authors',
        ]

class UpdateBookFrom(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            'name',
            'authors',
        ]
