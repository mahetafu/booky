from django.urls import path

from . import views

urlpatterns = [
    path('', views.books, name='books'),

    path('create-book/', views.create_book, name='create-book'),
    path('update-book/<int:book_id>/', views.update_book, name='update-book'),
    path('delete-book/<int:book_id>/', views.delete_book, name='delete-book'),

]
