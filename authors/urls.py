from django.urls import path

from . import views

urlpatterns = [
    path('', views.authors, name='authors'),

    path('create-author/', views.create_author, name='create-author'),
    path('update-author/<int:author_id>/', views.update_author, name='update-author'),
    path('delete-author/<int:author_id>/', views.delete_author, name='delete-author'),
]
