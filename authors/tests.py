from django.test import TestCase

from .models import Author

# Create your tests here.

class AuthorsTestCase(TestCase):

    def setUp(self) -> None:
        test_author = Author(name='Stephen', lastname='King')
        test_author.save()

        return super().setUp()
    

    def test_http_create_author(self):
        context = {
            'name': 'Johan', 
            'lastname': 'Rowling'
        }
        response = self.client.post('/authors/create-author', context, follow=True)

        status_code = response.status_code

        self.assertEqual(status_code, 200)

    def test_http_update_author(self):
        context = {
            'name': 'Johan', 
            'lastname': 'Rowling'
        }
        response = self.client.post('/authors/update-author/1', context, follow=True)

        status_code = response.status_code

        self.assertEqual(status_code, 200)

    def test_http_delete_author(self):
        context = {}
        response = self.client.post('/authors/delete-author/1', context, follow=True)

        status_code = response.status_code

        self.assertEqual(status_code, 200)