from django.urls import path

from . import views

urlpatterns = [
    path('', views.available_books, name='available-books'),
    
    path('borrow-book/<int:book_id>/', views.borrow_book, name='borrow-book'),
    path('borrowed-books/', views.borrowed_books, name='borrowed-books'),
    path('return-book/<int:borrow_id>/', views.return_book, name='return-book'),
    path('overdue-books/', views.overdue_books, name='overdue-books'),
]
