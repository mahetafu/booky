from django.shortcuts import render, redirect
from http.client import HTTPResponse
from django import forms

from .models import LibraryUser
from .forms import CreateLibraryUserFrom, UpdateLibraryUserFrom

# Create your views here.

redirect_url = 'library-users'
user_template = 'libraryusers/users.html'
form_template_path = 'libraryusers/user-form.html'

def create_user(request) -> HTTPResponse:
    return edit_library_user(request, form_template_path, redirect_url, CreateLibraryUserFrom)

def users(request) -> HTTPResponse:
    context = { 'users': LibraryUser.objects.all(), }
    return render(request, user_template, context)

def update_user(request, user_id):
    user = LibraryUser.objects.get(id=user_id)
    return edit_library_user(request, form_template_path, redirect_url, \
        UpdateLibraryUserFrom, user, 'Update')

def delete_user(request, user_id):
    to_del = LibraryUser.objects.get(id=user_id)
    to_del.delete()
    return redirect(redirect_url)


### utils
def edit_library_user(request, template_path, redirect_url, user_form:forms.ModelForm, \
    user_to_edit:LibraryUser = None, edit_action:str = 'Create') -> HTTPResponse:
    user_form, was_edited = obtain_form(request, user_form, user_to_edit)

    if was_edited:
        return redirect(redirect_url)    

    context = {
        'form': user_form,
        'view_text': edit_action,
    }

    return render(request, template_path, context)

def obtain_form(request, user_form: forms.ModelForm, \
    user_to_edit: LibraryUser = None) -> tuple([object, bool]):
    edit_form = user_form(instance=user_to_edit)
    was_edited = False

    if request.method == 'POST':
        edit_form = user_form(request.POST, instance=user_to_edit)
        
        if edit_form.is_valid():
            edit_form.save()
            was_edited = True

    return edit_form, was_edited
