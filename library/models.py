from django.db import models

from django.utils import timezone 
from datetime import timedelta

from books.models import Book
from libraryusers.models import LibraryUser

# Create your models here.
class BookBorrow(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    borrower = models.ForeignKey(LibraryUser, on_delete=models.CASCADE)
    date_borrowed = models.DateField(default=timezone.now)
    date_returned = models.DateField(default=None, null=True)
    date_return = models.DateField(default=(timezone.now() + timedelta(days=7)))

    def __str__(self) -> str:
        return f'{self.book}, {self.borrower}'
